The task:
=

User merger application
-
The goal of this task is to provide a console application that merges user data located in files in the "input" directory,  
and writes a single file as an output in the "output" directory that contains all users in the input files in ascended order by the user id using any of the input formats,
with the "last login" field in ISO 8601 datetime format.

You can see an example output in the "output" directory

Please consider potential future extensibility to include other input and output formats and sources in your design.

You can use any existing open source libraries and tools that make your task easier.


Please provide:  
- an object oriented solution to the problem  
- the running codebase with instructions of how to run it  
- a small description of your approach/design and considerations  
- any comments on parts you might have not implemented due to time constraints  
- tests  

Please send your solution back via email





